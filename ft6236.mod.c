#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x59760de, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x23bf5fc5, __VMLINUX_SYMBOL_STR(i2c_del_driver) },
	{ 0x481e5e2, __VMLINUX_SYMBOL_STR(i2c_register_driver) },
	{ 0x55559476, __VMLINUX_SYMBOL_STR(input_register_device) },
	{ 0xdff24d2b, __VMLINUX_SYMBOL_STR(devm_request_threaded_irq) },
	{ 0xff8effd8, __VMLINUX_SYMBOL_STR(input_mt_init_slots) },
	{ 0x6bc6ed2d, __VMLINUX_SYMBOL_STR(input_set_abs_params) },
	{ 0x2a1e750d, __VMLINUX_SYMBOL_STR(device_property_present) },
	{ 0xdae7a5f2, __VMLINUX_SYMBOL_STR(device_property_read_u32_array) },
	{ 0x65effd80, __VMLINUX_SYMBOL_STR(devm_input_allocate_device) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0x5cad051c, __VMLINUX_SYMBOL_STR(gpiod_set_value_cansleep) },
	{ 0xb1b0246, __VMLINUX_SYMBOL_STR(devm_gpiod_get_optional) },
	{ 0x7235c897, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x2e5810c6, __VMLINUX_SYMBOL_STR(__aeabi_unwind_cpp_pr1) },
	{ 0xdb7305a1, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xf7352903, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0x6a49bb61, __VMLINUX_SYMBOL_STR(input_mt_sync_frame) },
	{ 0x27189978, __VMLINUX_SYMBOL_STR(input_mt_report_slot_state) },
	{ 0xfee9418d, __VMLINUX_SYMBOL_STR(input_event) },
	{ 0xff3702a1, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xdc473295, __VMLINUX_SYMBOL_STR(i2c_smbus_read_i2c_block_data) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0xb1ad28e0, __VMLINUX_SYMBOL_STR(__gnu_mcount_nc) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("i2c:ft6236");
MODULE_ALIAS("of:N*T*Cfocaltech,ft6236*");
